package edu.binghamton.cs.hello;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameView extends View {
  private int frame;
  private Paint mBorderPaint = new Paint();
  private Paint mCirclePaint = new Paint();
  private Timer mTimer;
  private Canvas mCanvas;
  private ArrayList<Fruit> fruits = new ArrayList<Fruit>();
  private ArrayList<Swipe> swipes = new ArrayList<Swipe>();
  private Swipe swipe;
  private Random rng;
  private int[] colors = {
    0xFFFF6666,
    0xFFFFFF66,
    0xFF66FF66,
    0xFF66FFFF,
    0xFF6666FF,
    0xFFFF66FF,
  };

  public GameView(Context context, AttributeSet attributeSet) {
    super(context, attributeSet);
    rng = new Random(System.nanoTime());
    frame = 0;
    mBorderPaint.setColor(0xFF330066);
    mCirclePaint.setColor(0xFFFFFFFF);

    mTimer = new Timer();
    mTimer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        GameView.this.postInvalidate();

        frame += 1;

        if(frame % 60 == 0) {
          int msx = GameView.this.rng.nextInt(1000);
          int msy = GameView.this.rng.nextInt(1000);
          int siz = GameView.this.rng.nextInt(100);
          int color = GameView.this.rng.nextInt(GameView.this.colors.length);
          Fruit temp = new Fruit(getWidth() / 2, getHeight(), 50 + siz, GameView.this.colors[color]);
          temp.dx((msx - 500.0) / 250.0);
          temp.dy(-(17.0 + msy / 100.0));

          synchronized(GameView.this.fruits) {
            GameView.this.fruits.add(temp);
          }
        }
      }
    }, 0, 16);
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    int action = event.getAction() & MotionEvent.ACTION_MASK;
    if (action == MotionEvent.ACTION_DOWN) {
      this.swipe = new Swipe();

      int index = event.getActionIndex();
      float curX = event.getX(index);
      float curY = event.getY(index);
      this.swipe.add(new Point(curX, curY));

      this.postInvalidate();
    }

    else if(action == MotionEvent.ACTION_MOVE) {
      int index = event.getActionIndex();
      float curX = event.getX(index);
      float curY = event.getY(index);
      this.swipe.add(new Point(curX, curY));
    }

    else if(action == MotionEvent.ACTION_UP) {
      int index = event.getActionIndex();
      float curX = event.getX(index);
      float curY = event.getY(index);
      this.swipe.add(new Point(curX, curY));

      synchronized(this.fruits) {
        ArrayList<Fruit> rm = new ArrayList<Fruit>();

        for(Fruit f : this.fruits) {
          for(Point p : this.swipe.points) {
            if(f.touch(p)) {
              // remove it!
              rm.add(f);
              continue;
            }
          }
        }

        for(Fruit f : rm) {
          this.fruits.remove(f);
        }
      }

      this.swipes.add(swipe);
      this.swipe = null;
      this.postInvalidate();
    }

    return true;
  }
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    canvas.drawRect(20, 20, getWidth() - 20, getHeight() - 20, mBorderPaint);

    synchronized(this.fruits) {
      ArrayList<Fruit> rm = new ArrayList<Fruit>();

      for(Fruit f : this.fruits) {
        f.update();
        mCirclePaint.setColor(f.color());
        canvas.drawCircle((int)f.x(), (int)f.y(), (int)f.size(), mCirclePaint);
        if(f.y() > getHeight() + f.size()/2) {
          rm.add(f);
        }
      }

      for(Fruit f : rm) {
        this.fruits.remove(f);
      }
    }

    synchronized(this.swipes) {
      ArrayList<Swipe> rm = new ArrayList<Swipe>();

      for(Swipe s : this.swipes) {
        s.update();
        if(s.size() <= 1 || s.alpha <= 0) {
          rm.add(s);
          continue;
        }

        mCirclePaint.setColor(s.alpha << 24 | 0xDDDDDD);
        mCirclePaint.setStrokeWidth(4);
        for(int i=1; i<s.size(); i++) {
          canvas.drawLine((float)s.get(i-1).x, (float)s.get(i-1).y, (float)s.get(i).x, (float)s.get(i).y, mCirclePaint);
        }
      }

      for(Swipe s : rm) {
        this.swipes.remove(s);
      }
    }

  }
}
