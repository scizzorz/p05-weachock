package edu.binghamton.cs.hello;

import java.util.ArrayList;

public class Swipe {
  public ArrayList<Point> points = new ArrayList<Point>();
  public int alpha = 255;

  public void update() {
    this.alpha -= 4;
  }

  public int size() {
    return this.points.size();
  }

  public Point get(int i) {
    return this.points.get(i);
  }

  public void add(Point p) {
    this.points.add(p);
  }
}
