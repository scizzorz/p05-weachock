package edu.binghamton.cs.hello;

public class Point {
  public double x, y;

  public Point() {
    this.x = 0.0;
    this.y = 0.0;
  }

  public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public double dist(double x, double y) {
    double dx = this.x - x;
    double dy = this.y - y;

    return Math.sqrt(dx*dx + dy*dy);
  }

  public double dist(Point p) {
    return this.dist(p.x, p.y);
  }
}
