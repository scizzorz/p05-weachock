package edu.binghamton.cs.hello;

public class Fruit {
  private double x, y, size;
  private double dx, dy;
  private int color;

  public Fruit(double x, double y, double size, int color) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.color = color;
  }

  public boolean touch(double x, double y) {
    double dx = this.x - x;
    double dy = this.y - y;
    double dist = Math.sqrt(dx*dx + dy*dy);

    return (dist <= this.size);
  }

  public boolean touch(Point p) {
    return (p.dist(this.x, this.y) <= this.size);
  }

  public void update() {
    this.x += this.dx;
    this.y += this.dy;
    this.dy += 0.2;
  }

  public double x() {
    return this.x;
  }

  public double y() {
    return this.y;
  }

  public double dx() {
    return this.dx;
  }

  public double dy() {
    return this.dy;
  }

  public double size() {
    return this.size;
  }

  public int color() {
    return this.color;
  }

  public void x(double x) {
    this.x = x;
  }

  public void y(double y) {
    this.y = y;
  }

  public void dx(double dx) {
    this.dx = dx;
  }

  public void dy(double dy) {
    this.dy = dy;
  }
}
